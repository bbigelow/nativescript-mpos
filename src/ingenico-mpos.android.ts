import * as app from 'tns-core-modules/application';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import { Common } from './ingenico-mpos.common';


declare var com: any;
declare var java: any;
declare var android: any;

export class IngenicoMpos extends Common {
    devicePrefix = 'RP45';
    private ingenico: any = com.ingenico.mpos.sdk.Ingenico.getInstance();
    discoveredDevices: any[] = [];
    selectedDevice: any = null;

    deviceStatusHandler: any;
    btDeviceStatusHandler: any;
    searchListener: any;
    pairedAndSetup: boolean = false;

    clerkId: string = '001';


    initialize(apiKey: string, url: string, clientVersion: string): Promise<null> {
        return new Promise((resolve, reject) => {
            this.ingenico.initialize(app.android.context, url, apiKey, clientVersion);
            this.ingenico.setLogging(true);
            resolve();
        });
    }

    login(userName: string, password: string): Promise<null> {
        return new Promise((resolve, reject) => {
            this.ingenico.user().login(userName, password, new com.ingenico.mpos.sdk.callbacks.LoginCallback({
                done: function (responseCode: number, user: any) {
                    console.log(user);
                    if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                        reject(responseCode);
                    } else {
                        resolve();
                    }
                }
            }));
        });
    }

    listenForDevice(unplugCallBack: IUnplugCallback= null): Promise<null> {
        return new Promise((resolve, reject) => {
            this.ingenico.device().setDeviceType(com.roam.roamreaderunifiedapi.constants.DeviceType.RP450c);
            this.ingenico.device().initialize(app.android.context);

            // this.searchListener = new com.roam.roamreaderunifiedapi.callback.SearchListener({
            //     onAttach: () => {
            //         console.log('onAttach:');
            //     },
            //     onDeviceDiscovered: () => {
            //         console.log('onDeviceDiscovered:');
            //     },
            //     onDiscoveryComplete: () => {
            //         console.log('onDiscoveryComplete');
            //     }
            // });

            this.deviceStatusHandler = new com.roam.roamreaderunifiedapi.callback.DeviceStatusHandler({
                onConnected: (res: any) => {
                    console.log('registerConnectionStatusUpdates: onConnected: ', JSON.stringify(res));
                    // have them unplug
                    // save  ...once the bluetooth has paired once... save the pair object in the cache.
                    resolve();
                },
                onDisconnected: () => {
                    console.log('registerConnectionStatusUpdates: onDisconnected: ');
                    try {
                        this.ingenico.device().unregisterConnectionStatusUpdates(this.deviceStatusHandler);
                    } catch (e) {
                        //
                        console.log('Error calling unregisterConnectionStatusUpdates')
                    }
                    // if (this.selectedDevice) {
                    //     console.log('releasing device.');
                    // }
                    if (this.selectedDevice && unplugCallBack) {
                        this.ingenico.device().release(new com.roam.roamreaderunifiedapi.callback.ReleaseHandler({
                            done: function (responseCode: number, user: any) {
                                console.log('Release Handler...Done..');
                                unplugCallBack();
                                // if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                                //     reject(responseCode);
                                // } else {
                                //     resolve();
                                // }
                            }
                        }));
                    } else {
                        console.log('no device.. but disconnected');
                    }

                },
                onError: (err: any) => {
                    console.log('listenForDevice: DeviceStatusHandler: Error');
                    reject(err);
                }
            });
            this.ingenico.device().registerConnectionStatusUpdates(this.deviceStatusHandler);
            // console.log('trying search listener');
            // this.ingenico.device().search(true, this.searchListener, 1500);
        });
    }

    // turnDeviceOn(): Promise<null> {
    //     return new Promise((resolve, reject) => {
    //         this.ingenico.device().turnOnDeviceViaAudioJack(app.android.context,
    //             new com.ingenico.mpos.sdk.callbacks.TurnOnDeviceCallBack({
    //             success: () => {
    //                 console.log('turnOnDevice: Success');
    //             },
    //             failed: (result: string) => {
    //                 console.log('turnOnDevice: Failed:', result);
    //             },
    //             notSupported: () => {
    //                 console.log('turnOnDevice: NotSupported:');
    //             }
    //         }));
    //     })
    // }

    findReader(confirmationCallback: ConfirmationHandler): Promise<null> {
        return new Promise((resolve, reject) => {
            this.ingenico.device().requestPairing(new com.roam.roamreaderunifiedapi.callback.AudioJackPairingListenerWithDevice({
                onPairConfirmation: (readerPasskey: string, mobilePasskey: string, device: any) => {
                    confirmationCallback(readerPasskey, mobilePasskey)
                        .then((confirmed: boolean) => {
                            this.ingenico.device().confirmPairing(confirmed);
                        })
                        .catch((err) => {
                            console.log(err);
                            reject('Pairing confirmation failed');
                        });
                },
                onPairSucceeded: (device) => {
                    console.log('onPairSucceeded:', device);
                    this.ingenico.device().select(device);
                    this.selectedDevice = device;
                    resolve();
                },
                onPairNotSupported: () => {
                    reject('Pairing not supported by this device manager');
                },
                onPairFailed: (err: any) => {
                    console.log('onPairFailed:', err);
                    reject('Pairing failed: ' + JSON.stringify(err));
                }
            }));
        });
    }

    setupDevice(progressCallBack:ISetupProgressCallback): Promise<null> {
        return new Promise((resolve, reject) => {
            console.log('setupDevice::Start');
            this.ingenico.device().checkDeviceSetup(new com.ingenico.mpos.sdk.callbacks.CheckDeviceSetupCallback({
                done: (responseCode: number, isSetupRequired: boolean) => {
                    console.log('Setup Required?:', isSetupRequired);
                    if (isSetupRequired) {
                        console.log('setupDevice::isSetupRequired::true');
                        // this.ingenico.device().setup(new com.ingenico.mpos.sdk.callbacks.DeviceSetupCallback({
                        //     done: function (responseCode: number) {
                        //         if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                        //             this.pairedAndSetup = false;
                        //             reject(responseCode);
                        //         } else {
                        //             this.pairedAndSetup = true;
                        //             resolve();
                        //         }
                        //     }
                        // }));
                        this.ingenico.device().setup(new com.ingenico.mpos.sdk.callbacks.DeviceSetupWithProgressCallback({
                            done: (responseCode: number) => {
                                console.log('setupDevice::DeviceSetupWithProgressCallback::' + responseCode);
                                if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                                    this.pairedAndSetup = false;
                                    reject(responseCode);
                                } else {
                                    this.pairedAndSetup = true;
                                    resolve();
                                }
                            },
                            setupProgress: (current: number, total: number) => {
                                console.log('setupDevice::current: ' + current  + ' total: ' + total);
                                if (progressCallBack) {
                                    progressCallBack(current, total);
                                }
                            }
                        }));
                    } else {
                        if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                            this.pairedAndSetup = false;
                            reject(responseCode);
                        } else {
                            this.pairedAndSetup = true;
                            resolve();
                        }
                    }
                }
            }));
        });
    }

    getSelectedDevice() : any {
        return this.getDeviceStr(this.selectedDevice);
    }

    getDeviceStr(device: any) : string {
        return device.getName() + ',' + device.getIdentifier();
    }

    setDevice(device: any) : void {
        this.selectedDevice = null;
        let i = 0;
        while (i < this.discoveredDevices.length && this.selectedDevice == null) {

            if (this.getDeviceStr(this.discoveredDevices[i]) === device) {
                this.selectedDevice = this.discoveredDevices[i];
            } else {
                i++;
            }
        }
        if (!this.selectedDevice) {
            throw new Error("unable to find device:" + device);
        } else {
        // public Device(DeviceType deviceType, CommunicationType communicationType, String name, String identifier) {
        //         b = CommunicationType.AudioJack;
        //     let a:any = new com.roam.roamreaderunifiedapi.data.Device(this.selectedDevice.getDeviceType(),
        //         this.selectedDevice.getConnectionType(),
        //         this.selectedDevice.getName(),
        //         this.selectedDevice.getIdentifier());
            let a:any = this.selectedDevice;
            this.ingenico.device().select(a);
        }
    }

    blueToothConnect(): Promise<null> {
        return new Promise((resolve, reject) => {
            console.log('BlueToothConnect::Start');
            console.log('BlueToothConnect::SetDeviceType');
            this.ingenico.device().setDeviceType(com.roam.roamreaderunifiedapi.constants.DeviceType.RP450c);
            console.log('BlueToothConnect::Select Device...' + this.selectedDevice);
            this.ingenico.device().select(this.selectedDevice);
            this.btDeviceStatusHandler = new com.roam.roamreaderunifiedapi.callback.DeviceStatusHandler({
                onConnected: (res: any) => {
                    console.log('bt::registerConnectionStatusUpdates: onConnected: ', JSON.stringify(res));
                    // have them unplug
                    // save  ...once the bluetooth has paired once... save the pair object in the cache.
                    resolve();
                },
                onDisconnected: () => {
                    console.log('bt::registerConnectionStatusUpdates: onDisconnected: ');
                    // try {
                    //     this.ingenico.device().unregisterConnectionStatusUpdates(this.deviceStatusHandler);
                    // } catch (e) {
                    //     //
                    //     console.log('Error calling unregisterConnectionStatusUpdates')
                    // }
                    // if (this.selectedDevice) {
                    //
                    //     console.log('releasing device.');
                    // }
                    // if (this.selectedDevice && unplugCallBack) {
                    //     unplugCallBack();
                    // } else {
                    //     console.log('no device.. but unconnected');
                    // }

                },
                onError: (err: any) => {
                    console.log('listenForDevice: DeviceStatusHandler: Error');
                    reject(err);
                }
            });
            this.ingenico.device().registerConnectionStatusUpdates(this.btDeviceStatusHandler);
            this.ingenico.device().initialize(app.android.context);
            // console.log('trying search listener');
            // this.ingenico.device().search(true, this.searchListener, 1500);

        });

    }
    isWiredHeadsetOn(): boolean {
        var audioManager: any = app.android.context.getSystemService('audio');
        return audioManager.isWiredHeadsetOn();
    }

    findDevices() : Promise<any> {
        return new Promise((resolve, reject) => {
            this.discoveredDevices = [];
            console.log('calling search..');
            this.ingenico.device().setDeviceType(com.roam.roamreaderunifiedapi.constants.DeviceType.RP450c);
            this.ingenico.device().search(app.android.context, true, new java.lang.Long(1500), new com.roam.roamreaderunifiedapi.callback.SearchListener({
                onDeviceDiscovered: (device: any) => {
                    console.log('onDeviceDiscovered..' + device.toString());
                    if (device.getName().startsWith(this.devicePrefix)) {
                        this.discoveredDevices.push(device);
                        console.log('++Device Discovered:' + device.toString());
                    } else {
                        console.log('--Device Discovered:' + device.toString());
                    }
                },
                onDiscoveryComplete: () => {
                    console.log('Discovery Complete:');
                    resolve();
                }
            }));
        });
    }

    processCreditSale(saleRequest: SaleRequest, updateCB: ICardCallback): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.pairedAndSetup) {
                console.log('pairedAndSetup');
                let tokenRequest: any;
                var creditSaleTransactionRequest: any;
                console.log('pairedAndSetup.next', JSON.stringify(saleRequest));
                try {

                    /*


                    Amount amount, List products, String clerkID, String gpsLongitude, String gpsLatitude, String transactionGroupID, String transactionNotes,
                String merchantInvoiceID, Boolean showNotesAndInvoiceOnReceipt, TokenRequestParameters tokenRequestParameters, String customReference, Boolean isCompleted

                     String currency, Integer total, Integer subtotal, Integer tax, Integer discount, String discountDescription, Integer tip) {
            a = currency;
                     */
                    console.log('about to create amount');
                    let amount = new com.ingenico.mpos.sdk.data.Amount(
                        saleRequest.amount.currency,
                        new java.lang.Integer(saleRequest.amount.total * 100),
                        new java.lang.Integer(saleRequest.amount.subtotal * 100),
                        new java.lang.Integer(saleRequest.amount.tax * 100),
                        new java.lang.Integer(saleRequest.amount.discount * 100),
                        saleRequest.amount.discountDescription,
                        new java.lang.Integer(saleRequest.amount.tip * 100));
                    console.log('amount created');
                    /*- Product(String name, Integer price, String description, String image, Integer quantity) {*/
                    let productArr = new java.util.ArrayList();
                    console.log('product:before');
                    for (let p of saleRequest.products) {
                        let product = new com.ingenico.mpos.sdk.data.Product(p.name, new java.lang.Integer(p.price), p.description, '', new java.lang.Integer(p.quantity));
                        productArr.add(product);
                    }
                    /*
                    Amount amount, List products, String clerkID, String gpsLongitude, String gpsLatitude, String transactionGroupID, String transactionNotes,
                                String merchantInvoiceID, Boolean showNotesAndInvoiceOnReceipt, TokenRequestParameters tokenRequestParameters, String customReference, Boolean isCompleted

                     */
                    // let tokenRequest: any = new com.ingenico.mpos.sdk.data.TokenRequestParameters();
                    creditSaleTransactionRequest = new com.ingenico.mpos.sdk.request.CreditSaleTransactionRequest(
                        amount,
                        productArr,
                        this.clerkId,
                        saleRequest.gpsLongitude,
                        saleRequest.gpsLatitude,
                        saleRequest.transactionGroupID);

                    console.log('creditSaleTransactionRequest created..');
                    // alert('processCreditSale: creditSaleTransactionRequest ' + JSON.stringify(creditSaleTransactionRequest));
                    this.ingenico.payment().processCreditSaleTransactionWithCardReader(creditSaleTransactionRequest,
                        new com.ingenico.mpos.sdk.callbacks.TransactionCallback({
                            applicationSelection: (appList: any, applicationcallback: any) => {
                                reject('application selection not implemented');
                            },
                            done: (responseCode: number, transactionResponse: any) => {
                                if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                                    // alert('transaction response code: !=Success::' +
                                    //     this.getResponseCodeString(responseCode) + '::' + transactionResponse.getTransactionId() + '::' +
                                    //     JSON.stringify(transactionResponse));
                                    console.log('Sale Done with error..: transaction response code: !=Success::' +
                                        this.getResponseCodeString(responseCode) + '::' + transactionResponse.getTransactionId() +
                                        transactionResponse.toString() + '::' + JSON.stringify(transactionResponse));
                                    reject({ responseCode: responseCode, transactionResponse: transactionResponse });
                                } else {
                                    // alert(this.getResponseCodeString(responseCode) + '::' + JSON.stringify(transactionResponse));
                                    console.log('Sale Done: ' + this.getResponseCodeString(responseCode) + '::' +
                                        JSON.stringify(transactionResponse))
                                    resolve({ responseCode: responseCode, transactionResponse: transactionResponse });
                                }
                            },
                            updateProgress: (progressCode: number, extraMessage: string) => {
                                updateCB(progressCode, extraMessage);
                                console.log('Progress:', this.getProgressMessage(progressCode));
                                //alert('Progress:' + this.getProgressMessage(progressCode) + ':' + extraMessage)
                            }
                        }));
                } catch (e) {
                    console.log('Failed to createSaleTransactionRequest:' + e);
                    return reject('Failed to createSaleTransactionRequest:' + e);
                }
            } else {
                reject('Device has not been paired and setup.');
            }
        });
    }

    processDebitCardSale(saleRequest: SaleRequest, updateCB: ICardCallback): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.pairedAndSetup) {
                try {
                    console.log('about to create amount');
                    let amount = new com.ingenico.mpos.sdk.data.Amount(
                        saleRequest.amount.currency,
                        new java.lang.Integer(saleRequest.amount.total * 100),
                        new java.lang.Integer(saleRequest.amount.subtotal * 100),
                        new java.lang.Integer(saleRequest.amount.tax * 100),
                        new java.lang.Integer(saleRequest.amount.discount * 100),
                        saleRequest.amount.discountDescription,
                        new java.lang.Integer(saleRequest.amount.tip * 100));
                    console.log('amount created');
                    /*- Product(String name, Integer price, String description, String image, Integer quantity) {*/
                    let productArr = new java.util.ArrayList();
                    console.log('converting/creating products.');
                    for (let p of saleRequest.products) {``
                        let product = new com.ingenico.mpos.sdk.data.Product(p.name, new java.lang.Integer(p.price), p.description, '', new java.lang.Integer(p.quantity));
                        productArr.add(product);
                    }
                    console.log('converting/creating products. - finished');
                    let debitSaleTransactionRequest = new com.ingenico.mpos.sdk.request.DebitSaleTransactionRequest(
                        amount,
                        productArr,
                        // this.clerkId,
                        saleRequest.gpsLongitude,
                        saleRequest.gpsLatitude,
                        saleRequest.transactionGroupID);

                    console.log('calling transaction.');
                    this.ingenico.payment().processDebitSaleTransactionWithCardReader(debitSaleTransactionRequest,
                        new com.ingenico.mpos.sdk.callbacks.TransactionCallback({
                            applicationSelection: (appList: any, applicationcallback: any) => {
                                reject('application selection not implemented');
                            },
                            done: (responseCode: number, transactionResponse: any) => {
                                if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode
                                    && responseCode != 4926) {
                                    reject({ responseCode: responseCode, transactionResponse: transactionResponse });//4926 - signature required
                                } else {
                                    // alert(JSON.stringify(transactionResponse));
                                    resolve({ responseCode: responseCode, transactionResponse: transactionResponse });
                                }
                            },
                            updateProgress: (progressCode: number, extraMessage: string) => {
                                // console.log('Progress:', progressCode);
                                updateCB(progressCode, extraMessage);
                                console.log('Progress:', this.getProgressMessage(progressCode));
                                //alert('Progress:' + this.getProgressMessage(progressCode) + ':' + extraMessage)

                            }
                        }));
                } catch (e) {
                    console.log('Catch:' + e);
                }
            } else {
                reject('Device has not been paired and setup.');
            }
        });
    }

    signatureRequired(transactionResponse: any): boolean {
        if (transactionResponse == null) {
            return false;
        } else {
            if (transactionResponse.getCardVerificationMethod()
                == com.ingenico.mpos.sdk.constants.CardVerificationMethod.Signature) {
                return true;
            } else {
                return false;
            }
        }
    }

    getTransactionForSignature(): Promise<string> {
        return new Promise((resolve, reject) => {
            var pendingTransactionId: string = this.ingenico.payment().getReferenceForTransactionWithPendingSignature();
            if (pendingTransactionId) {
                resolve(pendingTransactionId);
            } else {
                reject();
            }
        });
    }

    /**
     * Upload a captured signature image.
     * @param image base64 encoded image
     */
    uploadSignature(transactionId: string, image: string): Promise<null> {
        return new Promise((resolve, reject) => {
            this.ingenico.user().uploadSignature(
                transactionId,
                image,
                new com.ingenico.mpos.sdk.callbacks.UploadSignatureCallback({
                    done: (responseCode: number) => {
                        if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                            reject('Could not upload signature: ' + responseCode);
                        } else {
                            resolve();
                        }
                    }
                })
            )
        });
    }
/*
http://schemas.nativescript.org/tns.xsd
 public void done(Integer responseCode,
                Integer totalMatches,
                List<TransactionHistorySummary> transactions) {
            Log.v(TAG, "GetTransactions::done::" + responseCode);
            Log.v(TAG, "GetTransactions total matches::" + totalMatches);
            mProgressDialogListener.hideProgress();
            if (ResponseCode.Success == responseCode) {
                showToast("Success");
                historyList = transactions;
                Log.v(TAG, historyList.toString());
                RecyclerView.Adapter adapter = new HistoryAdapter(historyList);
                rvSummaryList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                showToast("Failed");
            }
        }
 */
    getTransactionHistory(query: any) : Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let types: any = new java.util.ArrayList();
                types.add(com.ingenico.mpos.sdk.constants.TransactionType.CreditSale);
                // let qb: any = new com.ingenico.mpos.sdk.data.TransactionQueryBuilder();
                // qb.setTransactionTypes(types);
                // qb.setPastDays(new java.lang.Integer(5));
                // qb.setPageSize(new java.lang.Integer(10));
                // qb.setPageNumber(new java.lang.Integer(0));
                let newQuery: any = new com.ingenico.mpos.sdk.data.TransactionQuery(types,
                    new java.lang.Integer(5),
                    new java.lang.Integer(10),
                    new java.lang.Integer(0),
                    '', //startDate
                    '',//endDate
                    '', //clerkId
                    '', // invoice id
                    new java.util.ArrayList(), //transactionStatuses
                    new java.util.ArrayList());  //optionalFields
                console.log('before calling getTransactionHistory..inside. :: Page Size: ' + newQuery.getPageSize());
                this.ingenico.user().getTransactionHistory(newQuery, new com.ingenico.mpos.sdk.callbacks.GetTransactionHistoryCallback({
                    done: function (responseCode: number, totalMatches: number, transactions: any) {
                        console.log('GetTransactionHistory::done::' + responseCode + '::' + totalMatches);
                        if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                            reject({ responseCode: responseCode, transactions: transactions });
                        } else {
                            console.log('Test...' + transactions.size() + '::' + transactions.get(1).getTransactionId());
                            let historyList: TransactionHistorySummary[] = [];
                            for(let i = 0; i < transactions.size(); i++) {
                                let item = transactions.get(i);
                                // this.logMessage('history Item:' + item.toString());
                                let n: TransactionHistorySummary = {
                                    responseCode: item.getTransactionResponseCode().toString(),
                                    transactionId: item.getTransactionId().toString(),
                                    clerkId: item.getClerkId().toString(),
                                    amount: +item.getAmount().getTotal().toString(),
                                    approvedAmount: +item.getApprovedAmount().toString(),
                                    transactionType: item.getTransactionType().toString(),
                                    deviceTimestamp: item.getDeviceTimestamp().toString(),
                                    deviceTimezone: item.getDeviceTimezone().toString(),
                                    status: item.getTransactionStatus().toString()
                                };
                                historyList.push(n);
                            }
                            // this.transactionHistory = results.transactions;

                            resolve({ responseCode: responseCode, transactions: historyList });
                            //   resolve();
                        }
                    }
                }));
            } catch (e) {
                console.log('catch: getTransactions::'+ e);
            }
        });

    }

    voidTransaction(transactionId: string, clerkId: string, gpsLongitude: string, gpsLatitude: string, updateCB: ICardCallback): Promise<any> {
        return new Promise((resolve, reject) => {
            let request = new com.ingenico.mpos.sdk.request.VoidTransactionRequest(transactionId, clerkId, gpsLongitude, gpsLatitude);
            this.ingenico.payment().processVoidTransaction(request,
                                    new com.ingenico.mpos.sdk.callbacks.TransactionCallback({
                applicationSelection: (appList: any, applicationcallback: any) => {
                    reject('application selection not implemented');
                },
                done: (responseCode: number, transactionResponse: any) => {
                    if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                        // alert('void transaction response code: !=Success::' +
                        //     this.getResponseCodeString(responseCode) + '::' + transactionResponse.getTransactionId() + '::' +
                        //     JSON.stringify(transactionResponse));
                        console.log('Void transaction done with error..: transaction response code: !=Success::' +
                            this.getResponseCodeString(responseCode) + '::' + transactionResponse.getTransactionId() +
                            transactionResponse.toString() + '::' + JSON.stringify(transactionResponse));
                        reject({ responseCode: responseCode, transactionResponse: transactionResponse });
                    } else {
                        // alert(this.getResponseCodeString(responseCode) + '::' + JSON.stringify(transactionResponse));
                        console.log('Sale Done: ' + this.getResponseCodeString(responseCode) + '::' +
                            JSON.stringify(transactionResponse))
                        resolve({ responseCode: responseCode, transactionResponse: transactionResponse });
                    }
                },
                updateProgress: (progressCode: number, extraMessage: string) => {
                    updateCB(progressCode, extraMessage);
                    console.log('Progress:', this.getProgressMessage(progressCode));
                    //alert('Progress:' + this.getProgressMessage(progressCode) + ':' + extraMessage)
                }
            }));
        });
    }

    refundTransaction(transactionId: string,
                      amount: Amount,
                      clerkId: string,
                      gpsLongitude: string,
                      gpsLatitude: string,
                      updateCB: ICardCallback): Promise<any> {
        return new Promise((resolve, reject) => {
            let transactionAmount = new com.ingenico.mpos.sdk.data.Amount(
                amount.currency,
                new java.lang.Integer(amount.total * 100),
                new java.lang.Integer(amount.subtotal * 100),
                new java.lang.Integer(amount.tax * 100),
                new java.lang.Integer(amount.discount * 100),
                amount.discountDescription,
                new java.lang.Integer(amount.tip * 100));

            let request = new com.ingenico.mpos.sdk.request.
                        CreditRefundTransactionRequest(transactionId, transactionAmount,  clerkId, gpsLongitude, gpsLatitude);
            this.ingenico.payment().processCreditRefundTransaction(request,
                new com.ingenico.mpos.sdk.callbacks.TransactionCallback({
                    applicationSelection: (appList: any, applicationcallback: any) => {
                        reject('application selection not implemented');
                    },
                    done: (responseCode: number, transactionResponse: any) => {
                        if (com.ingenico.mpos.sdk.constants.ResponseCode.Success != responseCode) {
                            // alert('refund transaction response code: !=Success::' +
                            //     this.getResponseCodeString(responseCode) + '::' + transactionResponse.getTransactionId() + '::' +
                            //     JSON.stringify(transactionResponse));
                            console.log('refund transaction done with error..: transaction response code: !=Success::' +
                                this.getResponseCodeString(responseCode) + '::' + transactionResponse.getTransactionId() +
                                transactionResponse.toString() + '::' + JSON.stringify(transactionResponse));
                            reject({ responseCode: responseCode, transactionResponse: transactionResponse });
                        } else {
                            // alert(this.getResponseCodeString(responseCode) + '::' + JSON.stringify(transactionResponse));
                            console.log('Refund Done: ' + this.getResponseCodeString(responseCode) + '::' +
                                JSON.stringify(transactionResponse))
                            resolve({ responseCode: responseCode, transactionResponse: transactionResponse });
                        }
                    },
                    updateProgress: (progressCode: number, extraMessage: string) => {
                        updateCB(progressCode, extraMessage);
                        console.log('Progress:', this.getProgressMessage(progressCode));
                        //alert('Progress:' + this.getProgressMessage(progressCode) + ':' + extraMessage)
                    }
                }));
        });
    }

    getProgressMessage(progressMessage: number): string {
        switch (progressMessage) {
            case com.ingenico.mpos.sdk.constants.ProgressMessage.ApplicationSelectionCompleted:
                return "Application Selection Completed";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.ApplicationSelectionStarted:
                return "Application Selection Started";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.CardInserted:
                return "Card Inserted";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.CardHolderPressedCancelKey:
                return "Card Holder Pressed Cancel Key";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.DeviceBusy:
                return "Device Busy";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.ErrorReadingContactlessCard:
                return "Error Reading Contactless Card";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.FirstPinEntryPrompt:
                return "First Pin Entry Prompt";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.ICCErrorSwipeCard:
                return "ICC Error Swipe Card";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.LastPinEntryPrompt:
                return "Last Pin Entry Prompt";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PinEntryFailed:
                return "Pin Entry Failed";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PinEntryInProgress:
                return "Pin Entry In Progress";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PinEntrySuccessful:
                return "Pin Entry Successful";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PleaseInsertCard: {
                let posList: any = this.ingenico.device().allowedPOSEntryModes();
                if (posList.contains(com.ingenico.mpos.sdk.constants.POSEntryMode.ContactlessEMV) ||
                    posList.contains(com.ingenico.mpos.sdk.constants.POSEntryMode.ContactlessMSR)) {
                    if (posList.contains(com.ingenico.mpos.sdk.constants.POSEntryMode.ContactEMV)) {
                        return "Please insert/tap/swipe card";
                    }
                    else {
                        return "Please tap or swipe card";
                    }
                }
                else {
                    return "Please insert or swipe card";
                }
            }
            case com.ingenico.mpos.sdk.constants.ProgressMessage.RetryPinEntryPrompt:
                return "Retry Pin Entry";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.SwipeDetected:
                return "Swipe Detected";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.SwipeErrorReswipeMagStripe:
                return "Swipe Error Re-swipe MagStripe";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.TapDetected:
                return "Tap Detected";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.UseContactInterfaceInsteadOfContactless:
                return "Use Contact Interface Instead Of Contactless";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.WaitingforCardSwipe:
                return "Waiting for Card Swipe";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.RecordingTransaction:
                return "Recording Transaction";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.GettingOnlineAuthorization:
                return "Getting Payment Authorization";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.GettingCardVerification:
                return "Getting Card Verification";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.SendingReversal:
                return "Sending Reversal";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.UpdatingTransaction:
                return "Updating Transaction";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.RestartingContactlessInterface:
                return "Restarting Contactless Interface";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.TryContactInterface:
                return "Try Contact Interface";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PleaseRemoveCard:
                return "Please Remove Card";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PleaseSeePhone:
                return "Please See Phone";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.MultipleContactlessCardsDetected:
                return "Please Present One Card Only";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.PresentCardAgain:
                return "Please Present Card Again";
            case com.ingenico.mpos.sdk.constants.ProgressMessage.CardRemoved:
                return "Card Removed";
        }
        return "Progress Message:" + progressMessage;
    }

    getResponseCodeString(responseCode: number): string {
        switch (responseCode) {
            case com.ingenico.mpos.sdk.constants.ResponseCode.Success:
                return "Success";
            case com.ingenico.mpos.sdk.constants.ResponseCode.PaymentDeviceNotAvailable:
                return "Payment Device Not Available";
            case com.ingenico.mpos.sdk.constants.ResponseCode.PaymentDeviceError:
                return "Payment Device Not Error";
            case com.ingenico.mpos.sdk.constants.ResponseCode.PaymentDeviceTimeout:
                return "Payment Device Timeouts";
            case com.ingenico.mpos.sdk.constants.ResponseCode.NotSupportedByPaymentDevice:
                return "Not Supported by Payment Device";
            case com.ingenico.mpos.sdk.constants.ResponseCode.CardBlocked:
                return "Card Blocked";
            case com.ingenico.mpos.sdk.constants.ResponseCode.ApplicationBlocked:
                return "Application Blocked";
            case com.ingenico.mpos.sdk.constants.ResponseCode.InvalidCard:
                return "Invalid Card";
            case com.ingenico.mpos.sdk.constants.ResponseCode.InvalidApplication:
                return "Invalid Card Application";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionCancelled:
                return "Transaction Cancelled";
            case com.ingenico.mpos.sdk.constants.ResponseCode.CardReaderGeneralError:
                return "Card Reader General Error";
            case com.ingenico.mpos.sdk.constants.ResponseCode.CardInterfaceGeneralError:
                return "Card Not Accepted";
            case com.ingenico.mpos.sdk.constants.ResponseCode.BatteryTooLowError:
                return "Battery Too Low";
            case com.ingenico.mpos.sdk.constants.ResponseCode.BadCardSwipe:
                return "Bad Card Swipe";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionDeclined:
                return "Transaction Declined";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionReversalCardRemovedFailed:
                return "Transaction Reversal Card Removed Failed";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionReversalCardRemovedSuccess:
                return "Transaction Reversal Card Removed Success";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionReversalChipDeclineFailed:
                return "Transaction Reversal Chip Decline  Failed";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionReversalChipDeclineSuccess:
                return "Transaction Reversal Chip Decline Success";
            case com.ingenico.mpos.sdk.constants.ResponseCode.TransactionRefusedBecauseOfTransactionWithPendingSignature:
                return "Transaction Refused Because Of Transaction With Pending Signature";
            case com.ingenico.mpos.sdk.constants.ResponseCode.UnsupportedCard:
                return "Unsupported Card";
        }
        return responseCode.toString();
    }
}


export interface ConfirmationHandler {
    (readerPasskey: string, mobilePasskey: string): Promise<boolean>
}

export interface SaleRequest {
    amount: Amount;
    products: Product[];
    gpsLongitude: string;
    gpsLatitude: string;
    transactionGroupID: string;
}

export interface Amount {
    currency: string;
    total: number;
    subtotal: number;
    tax: number;
    discount: number;
    discountDescription: string;
    tip: number;
}

export interface Product {
    name: string;
    price: number;
    description: string;
    image: string;
    quantity: number;
}

export interface ICardCallback {
    (updateCode: number, extraMessage: string): void;
}

export interface IUnplugCallback {
    (): void;
}

export interface ISetupProgressCallback {
    (current: number, total: number): void;
}


export interface CreditSaleResponse {
    responseCode: any,
    transactionResponse: any
}

export interface TransactionHistoryResponse {
    responseCode: any,
    transactions: any[]
}

export interface TransactionHistorySummary {
    transactionType : string;
    transactionId : string;
    clerkId : string;
    amount : number;
    approvedAmount : number;
    deviceTimestamp : string;
    deviceTimezone: string;
    status: string;
    responseCode: string;
}